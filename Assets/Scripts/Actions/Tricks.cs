﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zoo;

public class Tricks : MonoBehaviour
{
    public static bool startTrick = false;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DoTrick()
    {
        Spawner.dora.PerformTrick();
        Spawner.wally.PerformTrick();
        Spawner.kali.PerformTrick();
    }
}


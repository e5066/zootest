﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zoo;

public class Hello : MonoBehaviour
{
    [SerializeField]
    GameObject input;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PerformHello()
    {
        switch (input.GetComponent<InputField>().text.ToLower())
        {
            case "tiger":
                Spawner.wally.SayHello();
                break;
            case "wally":
                Spawner.wally.SayHello();
                break;
            case "lion":
                Spawner.henk.SayHello();
                break;
            case "henk":
                Spawner.henk.SayHello();
                break;
            case "hippo":
                Spawner.elsa.SayHello();
                break;
            case "elsa":
                Spawner.elsa.SayHello();
                break;
            case "pig":
                Spawner.dora.SayHello();
                break;
            case "dora":
                Spawner.dora.SayHello();
                break;
            case "zebra":
                Spawner.marty.SayHello();
                break;
            case "marty":
                Spawner.marty.SayHello();
                break;
            case "chicken":
                Spawner.kali.SayHello();
                break;
            case "kali":
                Spawner.kali.SayHello();
                break;
            case "fox":
                Spawner.rena.SayHello();
                break;
            case "rena":
                Spawner.rena.SayHello();
                break;
            default:
                Spawner.wally.SayHello();
                Spawner.henk.SayHello();
                Spawner.dora.SayHello();
                Spawner.elsa.SayHello();
                Spawner.marty.SayHello();
                Spawner.kali.SayHello();
                Spawner.rena.SayHello();
                break;
        }
    }
}

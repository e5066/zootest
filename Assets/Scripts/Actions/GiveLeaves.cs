﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zoo;

public class GiveLeaves : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GiveLeaf()
    {
        Spawner.dora.EatLeaves();
        Spawner.elsa.EatLeaves();
        Spawner.marty.EatLeaves();
        Spawner.kali.EatLeaves();
    }
}

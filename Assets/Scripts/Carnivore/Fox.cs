﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Zoo
{
    public class Fox : MonoBehaviour
    {
        // Start is called before the first frame update
        [SerializeField]
        private GameObject Balloon;
        [SerializeField]
        private Text text;
        public string name;


        public void SayHello()
        {
            Balloon.SetActive(true);
            text.text = "Waaaahhh ghighighi";
        }

        public void EatMeat()
        {
            Balloon.SetActive(true);
            text.text = "ghlup glup slurp slurp";
        }
    }
}



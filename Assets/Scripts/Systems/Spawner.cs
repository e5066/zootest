﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Zoo
{
    class Spawner : MonoBehaviour
    {
        [SerializeField]
        private GameObject lion, hippo, pig, tiger, zebra, fox, chicken;
        public static Lion henk;
        public static Hippo elsa;
        public static Pig dora;
        public static Tiger wally;
        public static Zebra marty;
        public static Fox rena;
        public static Chicken kali;
        private void Start()
        {
            henk = Instantiate(lion, transform).GetComponent<Lion>();
            henk.name = "henk";
            elsa = Instantiate(hippo, transform).GetComponent<Hippo>();
            elsa.name = "elsa";
            dora = Instantiate(pig, transform).GetComponent<Pig>();
            dora.name = "dora";
            wally = Instantiate(tiger, transform).GetComponent<Tiger>();
            wally.name = "wally";
            marty = Instantiate(zebra, transform).GetComponent<Zebra>();
            marty.name = "marty";
            rena = Instantiate(fox, transform).GetComponent<Fox>();
            rena.name = "rena";
            kali = Instantiate(chicken, transform).GetComponent<Chicken>();
            kali.name = "kali";
        }
    }
}
